<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerGrpe4ja\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerGrpe4ja/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerGrpe4ja.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerGrpe4ja\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \ContainerGrpe4ja\appDevDebugProjectContainer(array(
    'container.build_hash' => 'Grpe4ja',
    'container.build_id' => 'aab74046',
    'container.build_time' => 1523700783,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerGrpe4ja');
